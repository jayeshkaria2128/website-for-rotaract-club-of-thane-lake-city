    /*********************************************************
            LOADER JS
    *********************************************************/
    $(window).on('load', function() {
        $(".loader").delay(2200).fadeOut('slow');
    });

    /*********************************************************
                    STATS JS
    *********************************************************/
    $(function() {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
        /*********************************************************
                    NAVIGATION BAR JS
        ****************************************************/
        showHideNav();
        $(window).scroll(function() {
            showHideNav();
        })

        function showHideNav() {
            if ($(window).scrollTop() > 50) {
                $('nav').addClass('scrolled-navbar green-nav-top');
                $('.navbar-brand img').attr('src', "img/logo/logo-dark.png");
                $("#back-to-top").fadeIn();
            } else {
                $('.navbar-brand img').attr('src', "img/logo/logo.png");
                $('nav').removeClass('scrolled-navbar green-nav-top');
                $("#back-to-top").fadeOut();
            }
        }


        /*********************************************************
                            MOBILE NAV JS
        *********************************************************/

        $('#mobile-nav-open-btn').click(function() {
            $('#mobile-nav').css("height", "100%");
        });

        $('#mobile-nav-close-btn').click(function() {
            $('#mobile-nav').css("height", "0%");
        });

    });




    /*********************************************************
                        CLIENT OWL-CAROUSEL JS
    *********************************************************/

    $(function() {
        $("#carousel-elements").owlCarousel({
            items: 4,
            autoplay: true,
            margin: 20,
            loop: true,
            nav: false,
            smartSpeed: 1000,
            autoplayHoverPause: false,
            dots: false,
            navText: ['<i class="lni-chevron-left-circle"></i>', '<i class="lni-chevron-right-circle"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                768: {
                    items: 4
                }

            }
        });


        /*********************************************************
                            ISOTOPE JS
        *********************************************************/

        $("#isotope-container").isotope({});

        $("#isotope-filters").on("click", "button", function() {
            //        $(".scrolled-navbar").zindex('-9999');

            //		document.getElementsByClassName("navbar").style.zIndex = "-9999";

            //		document.getElementsByClassName("green-nav-top").style.zIndex = "-9999";
            let filtervalue = $(this).attr("data-filter");
            //        console.log(filtervalue);

            $("#isotope-container").isotope({
                filter: filtervalue
            });
            $("#isotope-filters").find('.active').removeClass('active');

            $(this).addClass('active');


        });
        /*********************************************************
                    PORTFOLIO MAHNIFIC POPUP JS
        *********************************************************/

        $("#portfolio-wrapper").magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            },
            callbacks: {
                open: function() {
                    $(".scrolled-navbar").css('display', 'none');
                    $(".mfp-close").css("cursor", "pointer");
                },
                close: function() {
                    $(".scrolled-navbar").css('display', 'block');
                    $('#close-btn').on('click', function(event) {
                        event.preventDefault();
                        $.magnificPopup.close();
                    });

                }
            },
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
                opener: function(openerElement) {
                    return openerElement.is('img') ? openerElement : openerElement.find('img');
                }
            },


        });

    });

    /*********************************************************
                        SMOOTH SCROLL
    *********************************************************/

    $(function() {
        $("a.smooth-scroll").click(function(event) {
            event.preventDefault();
            var section_id = $(this).attr("href");
            $("html,body").animate({
                scrollTop: $(section_id).offset().top
            }, 1250, "easeInOutExpo")
        });
    });


    /*********************************************************
                        TEAM OWL-CAROUSEL JS
    *********************************************************/
    $(document).ready(function() {
        $("#t-owl").owlCarousel({
            items: 3,
            autoplay: true,
            loop: true,
            margin: 40,
            nav: true,
            smartSpeed: 1000,
            autoplayHoverPause: false,
            dots: false,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                768: {
                    items: 3
                }

            }
        })
    });


    /*********************************************************
                            ABOUT-US OWL-CAROUSEL JS
        *********************************************************/

    $(document).ready(function() {

        $("#owl-demo").owlCarousel({

            //navigation: true, // Show next and prev buttons
            slideSpeed: 400,
            paginationSpeed: 400,
            // singleItem: true,
            // items: 1,
            // itemsDesktop: false,
            // itemsDesktopSmall: false,
            itemsTablet: false,
            itemsMobile: false,

            items: 1,
            autoplay: true,
            margin: 20,
            loop: true,
            smartSpeed: 1000,
            autoplayHoverPause: false,
            dots: false,

        });

    });
    /*********************************************************
                    NAVBAR MOBILE JS
        *********************************************************/

    showHideNav();
    $(window).scroll(function() {
        showHideNav();
    })

    function showHideNav() {
        if ($(window).scrollTop() > 50) {
            $("#mobile-nav-open-btn").css("color", "#FFF");
            $("nav").addClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', 'img/logo/logo-dark.png');
            $(".btn-back-to-top").slideDown();
        } else {
            $("#mobile-nav-open-btn").css("color", "#F57C47");
            $("nav").removeClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', 'img/logo/logo.png');
            $(".btn-back-to-top").slideUp();
        }
    }

    $("#mobile-nav-open-btn").click(function() {
        $("#mobile-nav").css('height', '100%');
        $("#mobile-nav").css("z-index", 9999);
        $("#mobile-nav-content").css('width', '40%');
        $(".mobile-nav-content").css("display", "block");
        $("#mobile-nav-close-btn").css("display", "block");
        $(".nav-overlay-panel").css("display", "block");

    });

    $("#mobile-nav-close-btn").click(function() {
        $("#mobile-nav").css('height', '0%');
        $(".mobile-nav-content").css("display", "none");
        $("#mobile-nav-close-btn").css("display", "none");
        $(".nav-overlay-panel").css("display", "none");

    });



    new WOW().init();